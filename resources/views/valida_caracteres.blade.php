<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8"/>
        <title>Teste</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    </head>
    <body>
        <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
        <div class="container">
            <h1>Validador de Caracteres</h1>  
            <p>Escreva uma função que receba uma string contendo alguns caracteres e determine se a ordem é válida. Os caracteres considerados são representados da seguinte forma: (, ), {, }, [ ou ].
                Para ser considerado válido a entrada da função deve seguir as seguintes condições:</p>
            <ul>
                <li>Não deve conter caracteres incompatíveis;</li>
                <li>Um caractere de abertura deve possuir seu respectivo caractere de fechamento;</li>
                <li>Um subconjunto de caracteres deve ser uma sequencia válida de caracteres;</li>
            </ul> 
            <form action="/submit" method="POST">
                @csrf
                <div class="row">
                    <div class="col-12"><label for="texto">Digite seu texto</label></div>
                    <div class="col-10">
                        <input type="text" class="form-control" id="texto" name="texto" placeholder="Ex: [[{()}]] " value="{{old('texto')}}">
                    </div>
                    <div class="col-2">
                        <input type="submit" class="btn btn-group-sm btn-success" value="Validar" /> 
                    </div>
                </div>
                <div class="row p-2">  
                    @if($resposta)
                        <div class="alert alert-success" role="alert">{{ (!empty($mensagem))?$mensagem:'' }}</div>
                    @else  
                        <div class="alert alert-danger" role="alert">{{ (!empty($mensagem))?$mensagem:'' }}</div>
                    @endif  
                </div>
            </form>
        </div> 
    </body>
</html>