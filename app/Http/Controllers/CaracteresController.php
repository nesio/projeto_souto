<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CaracteresController extends Controller {

    public function index(Request $request) {
        $result = 0;
        return view('valida_caracteres',["resposta" => $result]);
    }
    
    public function submit(Request $request){
                
        $result = $this->validar($request->texto);
        $mensagem = ($result)?"Sequência válida - " . $request->texto:"Sequência inválida - " . $request->texto;
        return view('valida_caracteres',["resposta" => $result, "mensagem" => $mensagem]);
    }
    
    /**
     * 
     * @param type $texto
     * @return boolean
     */
    public function validar($texto) {
        
        $caracteresPermitidos = array("(", "[", "{", ")", "]", "}");
        $caracteresEntrada = array("(", "[", "{");
        $caracteresSaida = array(")", "]", "}");
        $totalCaracteres = strlen($texto);
        $caracteres = str_split($texto);
        $marcador = 0; 

        if ($totalCaracteres % 2 != 0)
            return false;
        
        if(empty($texto))
            return false;
        
        for ($i = 0; $i < $totalCaracteres; $i++) { 
            if(!in_array($caracteres[$i], $caracteresPermitidos))
               return false;
        }
        
        $fechamento;
        $arrayEntradas = array();
        for ($i = 0; $i < $totalCaracteres; $i++) { 
            if (isset($caracteres[$i]) && in_array($caracteres[$i], $caracteresEntrada)) {
                $arrayEntradas[$i] = $caracteres[$i];
            } 
        } 
                
        krsort($arrayEntradas); 
        foreach ($arrayEntradas as $k => $v){
            switch ($v) {
                case "[":
                    $fechamento = "]";
                    break;
                case "{":
                    $fechamento = "}";
                    break;
                case "(":
                    $fechamento = ")";
                    break;
                default :
            }
            
            //print_r($caracteres); echo '<br>';
            foreach ($caracteres as $key => $value){
                if($key > $k){ 
                    if($marcador != $k){
                        $marcador = $k; 
                        if(isset($caracteres[$key]) && $caracteres[$key] == $fechamento ){
                            //echo "Removendo as posições " .  $v . $k .','. $key . $caracteres[$key] . " do array <br><br>";
                            unset($caracteres[$k], $caracteres[$key]);
                        }else{
                            //echo "Sequência inválida " .  $v . $k .','. $key . $caracteres[$key] . " no array <br><br>";
                            return false;
                        } 
                    }
                }
            } 
        }
        
        if(count($caracteres) > 0){
            return false;
        }
                
        return true;
        
    }

}
